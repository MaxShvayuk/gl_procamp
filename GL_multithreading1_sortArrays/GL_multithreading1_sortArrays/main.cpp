#include <algorithm>
#include <iostream>
#include <vector>
#include <thread>
#include <chrono>
#include <pthread.h>
#include <semaphore.h>


int numberInOneArray = 5000;
int amountArrays = 160;//how much arrays should program sort ?
unsigned int coresNumber = 1;//how many cores does your CPU have?
int errorFlag = 0;//variable for detecting pthread errors
std::vector <std::vector<int> > arrayVectors;//vector of arrays which should be sorted

pthread_t *arrayThread ;//array of threads
void *thread_function(void *arg);//thread function where arrays are sorting

sem_t semaphore;//declarate semaphore

int counter = 0;//counter which show what number of arrays should to be sorted by current thread

int main(void)
{
    std::cout<<"how many arrays do you want to sort?\n";
    std::cin >> amountArrays;
    std::cout<<"how many integers are there in each array?\n";
    std::cin>>numberInOneArray;

    coresNumber = std::thread::hardware_concurrency();//count number of processor`s cores
    if(coresNumber == 0) {
        std::cerr<<"can`t count number of processor`s cores\n";
        exit(EXIT_FAILURE);
    }
    std::cout<<"number of CPU cores "<<coresNumber<<"\n\n";

    errorFlag = sem_init(&semaphore, 0, 1);//initialize semaphore
    if(errorFlag != 0) {
        std::cerr<<"Semaphore creation failed";
        exit(EXIT_FAILURE);
    }

    srand(time(0));
    for(int i=0; i<amountArrays; i++) {//filling vectors by random numbers
        arrayVectors.push_back(std::vector <int> ());
        for(int j=0; j<numberInOneArray; j++)
            arrayVectors.at(i).push_back( rand() % 10000);
    }

    arrayThread = new pthread_t[coresNumber];//allocate memory for array of threads
    for(unsigned int i=0; i<coresNumber; i++) {//declaration members of array of threads
        pthread_t thread;
        arrayThread[i] = thread;
    }

    auto beginTime = std::chrono::steady_clock::now();//start the stopwatch

    for(unsigned int i=0; i<coresNumber-1; i++) {//start all threads
        errorFlag = pthread_create( &arrayThread[i], NULL, thread_function, NULL);
        if(errorFlag != 0) {
            std::cerr<<"Thread creation failed";
            exit(EXIT_FAILURE);
        }
    }

    while(counter < amountArrays)
    {
        sem_wait(&semaphore);
        int local_counter = counter;
        counter++;
        sem_post(&semaphore);

        std::sort( arrayVectors.at(local_counter).begin(),  arrayVectors.at(local_counter).end() );
	//if(std::is_sorted( arrayVectors.at(local_counter).begin(),  arrayVectors.at(local_counter).end() ))
	//    std::cout<<"\narray "<<local_counter<<" is sorted"<<std::endl;
    }

    for(unsigned int i=0; i<coresNumber; i++) {//try to join all threads
        errorFlag = pthread_join(arrayThread[i], NULL);
        if(errorFlag != 0 && i<coresNumber-1){
            std::cerr<<"Thread join-failed";
            exit(EXIT_FAILURE);
        }
    }

    auto endTime = std::chrono::steady_clock::now();
    auto elapsedTimeMiliseconds = std::chrono::duration_cast <std::chrono::milliseconds> (endTime - beginTime);

    //std::cout<<"\n\nelapsed time =       "<<elapsedTimeMiliseconds.count()<<"       miliseconds\n\n"<<std::endl;
    //std::cout<<"\n\nelapsed time =       "<<elapsedTimeMiliseconds.count()<<"       miliseconds\n\n";
    std::cout<<"\n\nelapsed time = "<<elapsedTimeMiliseconds.count()<<"       miliseconds\n\n";

      sem_destroy(&semaphore);
    return 0;
}

void *thread_function(void *arg)
{
    int local_counter = 0;

    while(counter < amountArrays)
    {   //I use semaphore in binary-state (like mutex). Thread must wait to have access to counter variable
        sem_wait(&semaphore);
        local_counter = counter;
        counter++;
        sem_post(&semaphore);

        std::sort(arrayVectors.at(local_counter).begin(),  arrayVectors.at(local_counter).end() );
	//if(std::is_sorted( arrayVectors.at(local_counter).begin(),  arrayVectors.at(local_counter).end() ))
	//    std::cout<<"\narray "<<local_counter<<" is sorted"<<std::endl;
    }
    pthread_exit(NULL);//exit thread with return value NULL
}



