#include <iostream>
#include <thread>
#include <future>
#include <mutex>
#include <vector>
#include <chrono>
#include <opencv2/imgproc/imgproc.hpp>//for resize() function
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

const char* folderName = "wallpapers";//name of folder with srcImages - source Images
//cv::Mat srcImage;
cv::Mat resultImage;//image after resize
int cores_Number = 1;
cv::Size size(128, 128);
std::vector <std::future <void> > futuresArray;//array of std::future
std::vector <fs::path> arrayPath;//array with boost::filesystem::path members
std::mutex mutexCounter;
int pathCounter = 0;
int counter = 1;//we use this variable to indicate threadCutResizeAndMergeFunc() which of smallImages should be processed. It is thread-safety due to mutexCounter

void createArrayPath() {
    //create a vector with all path (names of srcImages). We use this pathes later, when we will upload srcImages using cv::imread()
    std::cout << "\nelements in folder:\n";

    for (fs::directory_iterator it(folderName); it != fs::directory_iterator(); ++it)
        arrayPath.push_back(*it);

    std::cout << "total "<<arrayPath.size()<<" images:\n";

    for(auto i : arrayPath)
        std::cout<<i<<"\n";
}

void setSizeOfResultImage() {//set the size of resultImage
    //    int resultWidth = 0;
    //    int resultHeight = 0;
    //    std::cout<<"set the width of result image: ";
    //    std::cin>>resultWidth;
    //    std::cout<<"set the height of result image: ";
    //    std::cin>>resultHeight;
    //    size = cv::Size(resultWidth, resultHeight);

    resultImage = cv::Mat(size, 16);//here we will piece together smallImages to create resizedImage(resultImage);
}

void countCpuCores() {
    cores_Number = std::thread::hardware_concurrency();//count how many cores does your PC have
    if(cores_Number == 0) std::cerr<<"\n\ncan`t count your CPU cores\n\n";

    std::cout<<", program uses "<<cores_Number<<" cores"<<std::endl;//print a number of CPU cores, which are used by this program to console
}

void asyncCutResizeAndMergeFunc(cv::Mat &srcImage) {

    int localCounter = 0;
    int w = srcImage.cols;//width of srcImage in pixels
    int hRoi = srcImage.rows / cores_Number;//height of each smallImage (heightROI)
    // ROI - region of interest of image

    while(counter < cores_Number) {
        mutexCounter.lock();
        localCounter = counter;
        counter++;
        std::cout<<localCounter<<"`s ID = "<<std::hex<<std::this_thread::get_id()<<std::dec<<std::endl;
        mutexCounter.unlock();

        cv::Rect roi(0, localCounter*hRoi, w, hRoi);//create smallImage
        cv::Mat tmpImage ( srcImage(roi) );

        cv::resize(tmpImage, tmpImage, size, 0, 0, cv::INTER_LINEAR);//resize smallImage

        //copy smallImages  to resultImage
        cv::Rect copyRect(0, tmpImage.rows * localCounter, tmpImage.cols, tmpImage.rows );
        tmpImage.copyTo(resultImage(copyRect) );
    }
}

void resizeImageInSeveralAsyncs(cv::Mat& srcImage) {

    for(int i=0; i<cores_Number-1; i++) { //create asyncs for each CPU core (cores_Number-1 because we already have main thread)
        std::cout<<"\nASYNC "<<i<<" STARNED\n";
        futuresArray.push_back(std::async(std::launch::async, asyncCutResizeAndMergeFunc, std::ref(srcImage)) );
    }

    std::cout<<"\nmain   = "<<std::hex<<std::this_thread::get_id()<<std::dec<<std::endl;

    cv::Rect roi(0, 0, srcImage.cols, srcImage.rows / cores_Number);//create smallImage
    cv::Mat tmpImage = srcImage(roi);

    cv::resize(tmpImage, tmpImage, size, 0, 0, cv::INTER_LINEAR);//resize smallImage

    cv::Rect copyRect(0, 0, tmpImage.cols, tmpImage.rows );//copy smallImages  to resultImage
    tmpImage.copyTo(resultImage(copyRect) );

    for(unsigned int i=0; i < futuresArray.size(); i++)  //wait for result of every future
        futuresArray.at(i).wait();

}

void drawAllImagesToGUI(cv::Mat& srcImage) {//create a windows, where we can see srcImage and resultImage
    cv::namedWindow( "srcImage", CV_WINDOW_AUTOSIZE );
    cv::namedWindow( "resultImage", CV_WINDOW_AUTOSIZE);

    cv::imshow( "srcImage", srcImage );
    cv::imshow( "resultImage" , resultImage );

    cv::waitKey(0);
}

void resizeAllImagesInFolder() {
    for(unsigned int i=0; i<arrayPath.size(); i++) {

        cv::Mat srcImage;

        srcImage = cv::imread( arrayPath.at(i).string(), CV_LOAD_IMAGE_COLOR );//upload srcImage in program
        if(!srcImage.data ) {//error checking
            std::cerr<<"ERROR: No image data \n ";
            std::terminate();
        }

        auto beginTime = std::chrono::high_resolution_clock::now();//start the stopwatch

        resizeImageInSeveralAsyncs(srcImage);//resize image in several threads

        counter = 1;

        auto endTime = std::chrono::high_resolution_clock::now();//stop the stopwatch
        auto elapsedTimeMicroseconds = std::chrono::duration_cast <std::chrono::microseconds> (endTime - beginTime);//count elapsed time
        std::cout<<"\nelapsed on resize time = "<<std::dec<<elapsedTimeMicroseconds.count()<<" microseconds\n\n";

        cv::imwrite("smallImages/smallImage" + std::to_string(i) + ".jpg", resultImage);//write resultImage to HDD

        //I don`t use this because drawing to GUI gigapixel images is very hard operation and my notebook hasn`t enough power to do this
        //drawAllImagesToGUI();

    }
}

int main( void)
{
    setSizeOfResultImage();

    createArrayPath();

    countCpuCores();
    std::cout<<", program uses "<<cores_Number<<" cores"<<std::endl;//print a number of CPU cores, which are used by this program to console

    size.height /= cores_Number;

    auto beginTime = std::chrono::high_resolution_clock::now();//start the stopwatch

    resizeAllImagesInFolder();

    auto endTime = std::chrono::high_resolution_clock::now();//stop the stopwatch
    auto elapsedTimeMilliseconds = std::chrono::duration_cast <std::chrono::milliseconds> (endTime - beginTime);//count elapsed time
    std::cout<<"\n\n\ntime, elapsed on resize all images in folder = "<<std::dec<<elapsedTimeMilliseconds.count()<<" miliseconds\n\n";

    return 0;
}
