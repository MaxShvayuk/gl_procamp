#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
#include <opencv2/imgproc/imgproc.hpp>//for resize() function
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

const char* srcImageName = "nasa.jpg";//name of srcImage - source Image
cv::Mat srcImage;
cv::Mat resultImage;//image after resize
int cores_Number = 1;
cv::Size size(6750, 1500);
std::vector < std::thread > threadArray;//array of threads
std::mutex mutexCounter;
int counter = 1;//we use this variable to indicate threadCutResizeAndMergeFunc() which of smallImages should be processed. It is thread-safety due to mutexCounter

void setSizeOfResultImage() {//set the size of resultImage
    int resultWidth = 0;
    int resultHeight = 0;
    std::cout<<"set the width of result image: ";
    std::cin>>resultWidth;
    std::cout<<"set the height of result image: ";
    std::cin>>resultHeight;
    size = cv::Size(resultWidth, resultHeight);

    resultImage = cv::Mat(size, 16);//here we will piece together smallImages to create resizedImage(resultImage);
}

void countCpuCores() {
    cores_Number = std::thread::hardware_concurrency();//count how many cores does your PC have
    if(cores_Number == 0) std::cerr<<"\n\ncan`t count your CPU cores\n\n";

    /* now we chose one of three modes:
  * - small image (less than 250 000 pixels) - program use maximum 2 cores
  * - medium image ( 250 000  -  3 000 000  pixels) - maximum 4 cores
  * - big image (more than 3 000 000 pixels) - no limit of cores number */
    std::cout<<"CPU has "<<cores_Number<<" cores";//print a number of avaiable CPU cores to consol
    int srcImagePixelsNumber = srcImage.cols * srcImage.rows;//number of pixels in srcImage

    if(srcImagePixelsNumber<=250000 && cores_Number > 2)
        cores_Number = 2;
    if(srcImagePixelsNumber>250000 && srcImagePixelsNumber <=3000000 && cores_Number > 4)
        cores_Number = 4;
    std::cout<<", program uses "<<cores_Number<<" cores"<<std::endl;//print a number of CPU cores, which are used by this program to console
}

void threadCutResizeAndMergeFunc() {

    int localCounter = 0;
    int w = srcImage.cols;//width of srcImage in pixels
    int hRoi = srcImage.rows / cores_Number;//height of each smallImage (heightROI)
    // ROI - region of interest of image

    while(counter < cores_Number) {
        mutexCounter.lock();
        localCounter = counter;
        counter++;
        mutexCounter.unlock();

        cv::Rect roi(0, localCounter*hRoi, w, hRoi);//create smallImage
        cv::Mat tmpImage ( srcImage(roi) );

        cv::resize(tmpImage, tmpImage, size, 0, 0, cv::INTER_LINEAR);//resize smallImage

        //copy smallImages  to resultImage
        cv::Rect copyRect(0, tmpImage.rows * localCounter, tmpImage.cols, tmpImage.rows );
        tmpImage.copyTo(resultImage(copyRect) );
    }
}

void resizeImageInSeveralThreads() {

    for(int i=0; i<cores_Number-1; i++)//create threads for each CPU core (cores_Number-1 because we already have main thread)
        threadArray.push_back(std::thread(threadCutResizeAndMergeFunc) );

    cv::Rect roi(0, 0, srcImage.cols, srcImage.rows / cores_Number);//create smallImage
    cv::Mat tmpImage = srcImage(roi);

    cv::resize(tmpImage, tmpImage, size, 0, 0, cv::INTER_LINEAR);//resize smallImage

    cv::Rect copyRect(0, 0, tmpImage.cols, tmpImage.rows );//copy smallImages  to resultImage
    tmpImage.copyTo(resultImage(copyRect) );

    for(unsigned int i=0; i < threadArray.size(); i++)  //try join all threads
        if(threadArray.at(i).joinable() )
            threadArray.at(i).join();
}

void drawAllImagesToGUI() {//create a windows, where we can see srcImage and resultImage
    cv::namedWindow( "srcImage", CV_WINDOW_AUTOSIZE );
    cv::namedWindow( "resultImage", CV_WINDOW_AUTOSIZE);

    cv::imshow( "srcImage", srcImage );
    cv::imshow( "resultImage" , resultImage );

    cv::waitKey(0);
}

int main( void)
{
    setSizeOfResultImage();

    srcImage = cv::imread( srcImageName, CV_LOAD_IMAGE_COLOR );//upload srcImage in program
    if(!srcImage.data ) {//error checking
        std::cerr<<" No image data \n ";
        return -1;
    }

    countCpuCores();

    size.height /= cores_Number;

    auto beginTime = std::chrono::high_resolution_clock::now();//start the stopwatch

    resizeImageInSeveralThreads();//resize image in several threads

    auto endTime = std::chrono::high_resolution_clock::now();//stop the stopwatch
    auto elapsedTimeMicroseconds = std::chrono::duration_cast <std::chrono::microseconds> (endTime - beginTime);//count elapsed time
    std::cout<<"\nelapsed on resize time = "<<elapsedTimeMicroseconds.count()<<" microseconds\n";

    cv::imwrite("images/resultImage.jpg", resultImage);//write resultImage to HDD

    //I don`t use this because drawing to GUI gigapixel images is very hard operation and my notebook hasn`t enough power to do this
    //drawAllImagesToGUI();

    return 0;
}
